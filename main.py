import os
import json
from textwrap import indent
from flask import Flask, render_template
from sqlalchemy import *
from sqlalchemy.orm import *
from flask_sqlalchemy import SQLAlchemy
from create_db import *
from flask_cors import CORS


CORS(app)

app.config['DEBUG'] = True
db.init_app(app)
# engine = create_engine(os.environ["SQLALCHEMY_DATABASE_URI"])
engine = create_engine('postgresql://postgres:asd123@35.194.51.252:5432/postgres')

# engine.connect()

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
  return render_template("index.html")

@app.route("/api/games")
def game_api():
    game_query = """select game.id, game.name, game.hypes, game.total_rating, game.slug, game.first_release_date, game.age_ratings, covers.image_id, covers.width, covers.height from public.game inner join covers on game.id = covers.game_id"""
    games = engine.execute(game_query)
    json_str =json.dumps([dict(game) for game in games], indent=4, sort_keys=True)
    
    response = app.response_class(
        response = json_str,
        status = 200,
        mimetype = 'application/json'
    )
    
    return response

@app.route("/api/devs")
def developers():
    query1 = "select companies.id, companies.name, companies.slug, companies.games_developed, companies.average_rating, companies.country, companies.first_release_date, company_logos.image_id from public.companies inner join company_logos on companies.logo = company_logos.id"
    devs = engine.execute(query1)
    json_str = json.dumps([dict(dev) for dev in devs], indent = 4, sort_keys=True)
    response = app.response_class(
        response = json_str,
        status = 200,
        mimetype = 'application/json'
    )
    
    return response
    
@app.route("/api/game-dev/")
@app.route("/api/game-dev/<id>/")
def game_dev(id=None):
    query1 = "select game.name as game_name, game.first_release_date as game_year, game.slug as game_slug, game.id as game_id, companies.name as dev_name, companies.slug as dev_slug, companies.id as company_id, covers.image_id as image_id from game inner join game_company on game.id = game_company.game inner join companies on game_company.company = companies.id inner join covers on game.id = covers.game_id;"
    game_dev_rels = engine.execute(query1)
    json_str = ""
    dict_arr = [dict(game_dev_rel) for game_dev_rel in game_dev_rels]
    if id and id.isnumeric():
        filtered_arr = []
        for i in range(len(dict_arr)):
            if int(dict_arr[i]["company_id"]) == int(id):
                # return dict1
                filtered_arr.append(dict_arr[i])
        json_str = json.dumps(filtered_arr, indent = 4, sort_keys=True)
    elif id:
        for i in range(len(dict_arr)):
            if dict_arr[i]["game_slug"] == id:
                json_str = dict_arr[i]
                break
        json_str = json.dumps(json_str)
    else :
        json_str = json.dumps(dict_arr, indent =4, sort_keys=True)
    
    response = app.response_class(
        response = json_str,
        status = 200,
        mimetype = 'application/json'
    )
    
    return response

@app.route("/api/game-platforms/")
@app.route("/api/game-platforms/<search>/")
def game_plats(search = None):
    game_plat_query ='''select game.id as game_id, game.name as game_name, game.slug as game_slug, covers.image_id as image_id, platform.id as platform_id, game.total_rating as game_rating, platform.abbreviation as abbrev, platform.slug as platform_slug, platform.name as platform_name, platform.id as platform_id from platform
    inner join game_platforms on platform.id = game_platforms.platform 
    right join game on game.id = game_platforms.game inner join covers on game.id = covers.game_id;'''
    game_plats = engine.execute(game_plat_query)
    dict_arr = [dict(game_plat) for game_plat in game_plats]
    json_str = ""
    if search and not search.isnumeric():
        filtered_arr = []
        for i in range(len(dict_arr)):
            if dict_arr[i]["game_slug"] == search:
                filtered_arr.append(dict_arr[i])
        json_str = json.dumps(filtered_arr, indent=4, sort_keys=True)
    elif search:
        filtered_arr = []
        for i in range(len(dict_arr)):
            if int(dict_arr[i]["platform_id"]) == int(search):
                filtered_arr.append(dict_arr[i])
        sortedArray = sorted(filtered_arr, key=lambda k: k["game_rating"])
        sortedArray.reverse()
        json_str = json.dumps(sortedArray, indent=4, sort_keys=True)
    else:
        json_str = json.dumps(dict_arr, indent=4, sort_keys=True)
    
    response = app.response_class(
        response = json_str,
        status = 200,
        mimetype = 'application/json'
    )
    
    return response

@app.route("/api/dev-platforms/")
@app.route("/api/dev-platforms/<search>")
def dev_plats(search = None):
    dev_plat_query ='''select platform.id as platform_id, companies.average_rating as company_rating, companies.id as developer_id, companies.slug as dev_slug, companies.name as company_name, company_logos.image_id as dev_logo from platform
    inner join game_platforms on platform.id = game_platforms.platform 
    inner join game on game.id = game_platforms.game
    inner join game_company on game_company.game = game.id inner join companies on companies.id = game_company.company inner join company_logos on company_logos.id = companies.logo'''
    dev_plats = engine.execute(dev_plat_query)
    
    dict_arr = [dict(dev_plat) for dev_plat in dev_plats]
    
    if search and search.isnumeric():
        filteredArr = []
        for i in range(len(dict_arr)):
            if int(dict_arr[i]["platform_id"]) == int(search):
                filteredArr.append(dict_arr[i])
        sortedArr = sorted(filteredArr, key=lambda k: k["company_rating"])
        sortedArr.reverse()
        
       
        
        
        json_str = json.dumps(sortedArr, indent=4, sort_keys=True)
    else:
        json_str = json.dumps(dict_arr, sort_keys=True, indent=4)
    response = app.response_class(
        response = json_str,
        status = 200,
        mimetype = 'application/json'
    )
    
    return response
    

@app.route("/api/platforms")
def platform_api():
      platform_query = """select platform.name, platform.abbreviation as abbrev, platform.id, platform.generation, platform.slug, platform.platform_family, platform.alternative_name, platform.year_released, platform.manufacturer, platform.platform_image_url from public.platform"""
      platforms = engine.execute(platform_query)
      json_str =json.dumps([dict(platform) for platform in platforms], indent=4, sort_keys=True)
      
      response = app.response_class(
            response = json_str,
            status = 200,
            mimetype = 'application/json'
      )
      
      return response

      
    

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, threaded=True, debug=True)
