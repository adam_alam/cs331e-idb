import React from "react";
import "./App.css";
import Navbar1 from "./components/Nav.jsx";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import GameDetail from "./components/GameDetail";
import AboutMe from "./pages/about/About";
import GameTablePage from "./components/GameTable3";
import PlatformTablePage from "./components/PlatformTable";
import DevTable from "./components/DevTable";
import DevDetail from "./components/DevDetail";
import PlatformDetail from "./components/PlatformDetail";
import Splash from "./pages/splash/splash";
import Test from "./pages/about/Test";
import Results from "./components/Results";

function App() {
  return (
    <React.Fragment>
      <Router>
        <Navbar1 />
        <Switch>
          <Route path="/" component={Splash} exact />
          <Route path="/games" component={GameTablePage} exact />
          <Route path="/developers" component={DevTable} exact />
          <Route path="/developers/:dev_slug" component={DevDetail} exact />
          <Route path="/platforms" component={PlatformTablePage} exact />
          <Route
            path="/platforms/:plat_slug"
            component={PlatformDetail}
            exact
          />
          <Route path="/games/:game_slug" component={GameDetail} />
          <Route path="/about_us" component={AboutMe} />
          <Route path="/unittests" component={Test} exact />
          <Route path="/search/:query" component={Results} />
          <Route path="/search/" component={Results} />
        </Switch>
      </Router>
    </React.Fragment>
  );
}

export default App;
