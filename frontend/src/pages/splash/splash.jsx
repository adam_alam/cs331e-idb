import React, { Component } from "react";
import GameVideo from "./Game2.mp4";
import { Link } from "react-router-dom";

class Splash extends Component {
  render() {
    return (
      <React.Fragment>
        <div>
          <video
            className="hidden-sm-down"
            style={{
              position: "fixed",
              right: 0,
              bottom: 0,
              minWidth: "100%",
              minHeight: "100%",
              objectFit: "cover",
            }}
            playsInline="playsinline"
            autoPlay="autoplay"
            muted="muted"
            loop="loop"
          >
            <source src={GameVideo} type="video/mp4" />
          </video>
        </div>

        <div id="intro">
          <h1 className="intro-header" id="ga-title">
            GameArchive
          </h1>
          <h3 className="intro-header">
            A Gaming Database for Your Favorite Video Games, Platforms, and
            Developers
          </h3>
        </div>
      </React.Fragment>
    );
  }
}

export default Splash;
