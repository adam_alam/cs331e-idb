import React, { Component } from "react";
import { Link } from "react-router-dom";
import Test from "./Test";
import Adam from "./adam.png";
import Avery from "./avery.png";
import Haris from "./haris.png";
import Chris from "./chris.png";
import Rani from "./rani.png";
import "./about_styles.css";

class AboutMe extends Component {
  render() {
    return (
      <React.Fragment>
        <h1 id="about-title">About Us</h1>

        <div className="row justify-content-center" id="main_div">
          <div className="card">
            <div className="card-body">
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <div className="card-title">
                    <h2>Avery Shepherd</h2>
                    <img
                      className="card-img-top center-img"
                      src={Avery}
                      alt="Avery"
                    />
                    <p className="card-text">
                      Avery is interested in primatology and data analytics.
                    </p>
                  </div>
                </li>

                <li className="list-group-item">
                  <h4>Major Responsibilities</h4>
                  <ul>
                    <li>Team leader</li>
                    <li>Created Logos</li>
                    <li>GCP Deployment</li>
                  </ul>
                </li>

                <li className="list-group-item">
                  <div className="card-body">
                    <p>Commits: 11</p>
                    <p>Issues: 6</p>
                    <p>Unit Tests: 5</p>
                  </div>
                </li>

                <li className="list-group-item">
                  <a
                    href="https://www.linkedin.com/in/avery-shepherd/"
                    className="btn btn-secondary btn-md"
                  >
                    Linkedin
                  </a>
                  <a
                    href="https://gitlab.com/averyshepherd"
                    className="btn btn-secondary btn-md"
                  >
                    Gitlab
                  </a>
                  <a
                    href="mailto:ashepherd@utexas.edu"
                    className="btn btn-secondary btn-md"
                  >
                    Email
                  </a>
                </li>
              </ul>
            </div>
          </div>

          <div className="card">
            <div className="card-body">
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <div className="card-title">
                    <h2>Adam Alam</h2>
                    <img
                      className="card-img-top center-img"
                      src={Adam}
                      alt="Adam"
                    />
                    <p className="card-text">
                      Adam is a UT Economics student with an interest in
                      Software Engineering.
                    </p>
                  </div>
                </li>

                <li className="list-group-item">
                  <h4>Major Responsibilities</h4>
                  <ul>
                    <li>Full-stack Developer</li>
                    <ul>
                      <li>Pagination and sorting for all models</li>
                      <li>
                        Creating and managing API to serve data to ReactJS
                        through Flask
                      </li>
                      <li>Creating all front end and React components</li>
                      <li>
                        Creating and implementing site-wide search
                        funcationality
                      </li>
                      <li>Styling pages</li>
                    </ul>
                  </ul>
                </li>

                <li className="list-group-item">
                  <div className="card-body">
                    <p>Commits: 108</p>
                    <p>Issues: 30</p>
                    <p>Unit Tests: 5</p>
                  </div>
                </li>

                <li className="list-group-item">
                  <a
                    href="https://www.linkedin.com/in/adambalam/"
                    className="btn btn-secondary btn-md"
                  >
                    Linkedin
                  </a>
                  <a
                    href="https://gitlab.com/adam_alam"
                    className="btn btn-secondary btn-md"
                  >
                    Gitlab
                  </a>
                  <a
                    href="mailto:adamalam7@outlook.com"
                    className="btn btn-secondary btn-md"
                  >
                    Email
                  </a>
                </li>
              </ul>
            </div>
          </div>

          <div className="card">
            <div className="card-body">
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <div className="card-title">
                    <h2>Rani Patel</h2>
                    <img
                      className="card-img-top center-img"
                      src={Rani}
                      alt="Rani"
                    />
                    <p className="card-text">
                      Rani is a senior biochemistry student at UT also working
                      on the Elements of Computing Certificate.
                    </p>
                  </div>
                </li>

                <li className="list-group-item">
                  <h4>Major Responsibilities</h4>
                  <ul>
                    <li>Backend Developer</li>
                    <ul>
                      <li>Creating and populating tables in database</li>
                      <li>Unittests</li>
                      <li>About Us</li>
                    </ul>
                  </ul>
                </li>

                <li className="list-group-item">
                  <div className="card-body">
                    <p>Commits: 36</p>
                    <p>Issues: 15</p>
                    <p>Unit Tests: 17</p>
                  </div>
                </li>

                <li className="list-group-item">
                  <a
                    href="https://www.linkedin.com/in/rani-patel-331414200/"
                    className="btn btn-secondary btn-md"
                  >
                    Linkedin
                  </a>
                  <a
                    href="https://gitlab.com/ranip99"
                    className="btn btn-secondary btn-md"
                  >
                    Gitlab
                  </a>
                  <a
                    href="mailto:ranipatel@utexas.edu"
                    className="btn btn-secondary btn-md"
                  >
                    Email
                  </a>
                </li>
              </ul>
            </div>
          </div>

          <div className="card">
            <div className="card-body">
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <div className="card-title">
                    <h2>Haris Bhatti</h2>
                    <img
                      className="card-img-top center-img"
                      src={Haris}
                      alt="Haris"
                    />
                    <p className="card-text">
                      Haris is a 4th year Advertising Major with a certificate
                      in CS and Business. His interests lie heavily in AR and VR
                      technologies, skateboarding, and making semi-humorous
                      videos for his friends.
                    </p>
                  </div>
                </li>

                <li className="list-group-item">
                  <h4>Major Responsibilities</h4>
                  <ul>
                    <li>Front End Developer</li>
                    <ul>
                      <li>Styling Games Instance Page</li>
                    </ul>
                  </ul>
                </li>

                <li className="list-group-item">
                  <div className="card-body">
                    <p>Commits: 7</p>
                    <p>Issues: 1</p>
                    <p>Unit Tests: 3</p>
                  </div>
                </li>

                <li className="list-group-item">
                  <a
                    href="https://www.linkedin.com/in/harisbhatti/"
                    className="btn btn-secondary btn-md"
                  >
                    Linkedin
                  </a>
                  <a
                    href="https://gitlab.com/haris.bhatti"
                    className="btn btn-secondary btn-md"
                  >
                    Gitlab
                  </a>
                  <a
                    href="mailto:haris.bhatti@utexas.edu"
                    className="btn btn-secondary btn-md"
                  >
                    Email
                  </a>
                </li>
              </ul>
            </div>
          </div>

          <div className="card">
            <div className="card-body">
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <div className="card-title">
                    <h2>Christopher Calizzi</h2>
                    <img
                      className="card-img-top center-img"
                      src={Chris}
                      alt="Christopher"
                    />
                    <p className="card-text">
                      Chris is a sophomore UT chemical engineering student
                      pursuing a certificate in computer science. Interested in
                      a career combining software with an engineering technical
                      background.
                    </p>
                  </div>
                </li>

                <li className="list-group-item">
                  <h4>Major Responsibilities</h4>
                  <ul>
                    <li>Backend Developer</li>
                    <ul>
                      <li>Obtained specific data from API</li>
                      <li>
                        Processed API data to make database more efficient
                      </li>
                    </ul>
                  </ul>
                </li>

                <li className="list-group-item">
                  <div className="card-body">
                    <p>Commits: 11</p>
                    <p>Issues: 6</p>
                    <p>Unit Tests: 4</p>
                  </div>
                </li>

                <li className="list-group-item">
                  <a
                    href="https://www.linkedin.com/in/christopher-calizzi-024441148/"
                    className="btn btn-secondary btn-md"
                  >
                    Linkedin
                  </a>
                  <a
                    href="https://gitlab.com/calizzic"
                    className="btn btn-secondary btn-md"
                  >
                    Gitlab
                  </a>
                  <a
                    href="mailto:calizzic@gmail.com"
                    className="btn btn-secondary btn-md"
                  >
                    Email
                  </a>
                </li>
              </ul>
            </div>
          </div>

          <div className="card">
            <div className="card-body">
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <div className="card-title">
                    <h2 id="our-website">Our Website</h2>
                  </div>
                </li>

                <li className="list-group-item">
                  <Link to="/unittests">
                    <button className="btn btn-secondary btn-lg btn-block">
                      Unittests
                    </button>
                  </Link>
                </li>
                <li className="list-group-item">
                  <a href="https://speakerdeck.com/ranip99/game-archive-presentation">
                    <button className="btn btn-secondary btn-lg btn-block">
                      Presentation
                    </button>
                  </a>
                </li>

                <li className="list-group-item">
                  <h4 className="card-img-top">Statistics</h4>
                  <ul>
                    <li>Total Commits: 173</li>
                    <li>Total Issues: 58</li>
                    <li>Total Unit Tests: 34</li>
                  </ul>
                </li>

                <li className="list-group-item card-img-top">
                  <a
                    href="https://gitlab.com/adam_alam/cs331e-idb"
                    className="btn btn-secondary btn-sm"
                  >
                    Git Project
                  </a>
                  <a
                    href="https://gitlab.com/adam_alam/cs331e-idb/-/issues"
                    className="btn btn-secondary btn-sm"
                  >
                    Git Issues
                  </a>
                  <a
                    href="https://gitlab.com/adam_alam/cs331e-idb/-/wikis/Introduction"
                    className="btn btn-secondary btn-sm"
                  >
                    Git Wiki
                  </a>
                  <a
                    href="https://documenter.getpostman.com/view/15091338/TzJx8wQ4"
                    className="btn btn-secondary btn-sm"
                  >
                    Postman API Documentation
                  </a>
                </li>

                <li className="list-group-item card-img-top">
                  <h4>Tools</h4>
                  <ul>
                    <li>Gitlab</li>
                    <li>Slack</li>
                    <li>Flask</li>
                    <li>Google Cloud Platform</li>
                    <li>React</li>
                    <li>NPM</li>
                    <li>Bootstrap</li>
                    <li>SQLAlchemy</li>
                    <li>PostgreSQL</li>
                  </ul>
                </li>

                <li className="list-group-item card-img-top">
                  <a
                    href="https:/api-docs.igdb.com/#breaking-changes"
                    className="btn btn-secondary btn-md"
                  >
                    API 1
                  </a>
                  <a
                    href="https://www.mobygames.com/info/api"
                    className="btn btn-secondary btn-md"
                  >
                    API 2
                  </a>
                  <a
                    href="https://rawg.io/apidocs"
                    className="btn btn-secondary btn-md"
                  >
                    API 3
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AboutMe;
