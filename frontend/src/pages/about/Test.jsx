import React, { Component } from "react";
import Tests from "./test.png";


class Test extends Component {
  render() {
    return (
      <React.Fragment>
        <h1>Unit Tests</h1>
        <img src={Tests} alt = "unittests" class="rounded mx-auto d-block"></img>

      </React.Fragment>
    );
  }
}

export default Test;
