import React, { Component } from "react";
import { MDBDataTable } from "mdbreact";
import PacMan from "./../Pacman.svg";

class GameTablePage extends Component {
  handleClick = (slug) => {
    window.location.href = "/games/" + slug;
  };
  state = {
    loading: true,
    games: null,
    tableDataColumns: [
      {
        label: "Name",
        field: "name",
        sort: "asc",
        width: 150,
      },
      {
        label: "Metacritic Score",
        field: "total_rating",
        sort: "asc",
        width: 30,
      },
      {
        label: "Release Year",
        field: "first_release_date",
        sort: "asc",
        width: 200,
      },
      {
        label: "ESRB Rating",
        field: "age_ratings",
        sort: "asc",
        width: 200,
      },
      {
        label: "Hype Score",
        field: "hypes",
        sort: "asc",
        width: 100,
      },
    ],
    mdbData: null,
  };

  async componentDidMount() {
    const url = "/api/games";
    const response = await fetch(url);
    let data = await response.json();
    data.forEach((game) => {
      game.clickEvent = () => this.handleClick(game.slug);
      game.total_rating = game.total_rating.toFixed(1);
      if (game.hypes === null) {
        game.hypes = 0;
      }
    });
    this.setState({
      games: data,
      loading: false,
      mdbData: { columns: this.state.tableDataColumns, rows: data },
    });
  }
  render() {
    return (
      <div id="main-table-body">
        {this.state.loading || !this.state.games ? (
          <div>
            <img src={PacMan}></img>
            <br></br>
            <h3>Loading...</h3>
          </div>
        ) : (
          <React.Fragment>
            <h2>Games</h2>
            <div id="dataTable">
              <MDBDataTable
                striped
                bordered
                hover
                small
                data={this.state.mdbData}
                searching={true}
              />
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default GameTablePage;
