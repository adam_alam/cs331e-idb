import React, { Component } from "react";
import PacMan from "./../Pacman.svg";
import { Link } from "react-router-dom";
class PlatformInstance extends Component {
  state = {
    gamesLoading: true,
    devsLoading: true,
    topDevs: null,
    topGames: null,
  };
  async componentDidMount() {
    const url1 = `/api/game-platforms/${this.props.data.id}`;
    const url2 = `/api/dev-platforms/${this.props.data.id}`;
    const response2 = await fetch(url2);
    let data2 = await response2.json();
    const response1 = await fetch(url1);
    let data1 = await response1.json();
    let to_return = [];
    let devIds = [];
    for (let i = 0; i < data2.length; i++) {
      if (!devIds.includes(data2[i].developer_id)) {
        devIds.push(data2[i].developer_id);
        to_return.push(data2[i]);
      }
    }
    data2 = to_return.slice(0, 6);
    data1 = data1.slice(0, 6);
    this.setState({
      topGames: data1,
      gamesLoading: false,
      topDevs: data2,
      devsLoading: false,
    });
  }
  render() {
    const {
      abbrev,
      id,
      image_id,
      name,
      generation,
      manufacturer,
      alternative_name,
      year_released,
      platform_image_url,
    } = this.props.data;
    return (
      <div className="platform-div">
        <h2>{name}</h2>
        <img
          className="platform-img"
          src={platform_image_url}
          alt="Could Not Load Image"
          className="platform-cover"
        ></img>

        <h3>Year released: {year_released}</h3>
        <h3>Manufacturer: {manufacturer}</h3>
        <h3>Generation: {generation}</h3>
        <h3>Alternative Name: {alternative_name.toUpperCase()}</h3>
        {this.state.gamesLoading || this.state.devsLoading ? (
          <React.Fragment>
            <img src={PacMan}></img>
            <p>Loading</p>
          </React.Fragment>
        ) : (
          <div>
            <h2>Top Rated Games on {abbrev}</h2>
            <div className="container">
              <div className="row">
                {this.state.topGames.map((game) => (
                  <div className="col-md-6 col-lg-4">
                    <div className="card">
                      <img
                        className="card-img-top"
                        src={`http://images.igdb.com/igdb/image/upload/t_1080p_2x/${game.image_id}.png`}
                        alt="Card image cap"
                      />
                      <div className="card-body">
                        <h5 className="card-title">{game.game_name}</h5>
                        <p className="card-text">
                          Metacritic Score: {game.game_rating.toFixed(1)}
                        </p>
                        <Link
                          to={`/games/${game.game_slug}`}
                          className="btn btn-primary"
                        >
                          View Details
                        </Link>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        )}
        <br />
        <h2>Top Rated Developers on {abbrev}</h2>
        <div className="container">
          <div className="row">
            {this.state.devsLoading ? (
              <React.Fragment>
                <img src={PacMan}></img>
                <p>Loading Developers</p>
              </React.Fragment>
            ) : (
              this.state.topDevs.map((dev) => (
                <div className="col-md-6 col-lg-4">
                  <div className="card">
                    <img
                      className="card-img-top devCard-img"
                      src={`http://images.igdb.com/igdb/image/upload/t_1080p_2x/${dev.dev_logo}.png`}
                      alt="Card image cap"
                    />
                    <div className="card-body">
                      <h5 className="card-title">{dev.company_name}</h5>
                      <p className="card-text">
                        {dev.company_rating.toFixed(2)}
                      </p>
                      <Link
                        to={`/developers/${dev.dev_slug}`}
                        className="btn btn-primary"
                      >
                        View {dev.company_name}
                      </Link>
                    </div>
                  </div>
                </div>
              ))
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default PlatformInstance;
