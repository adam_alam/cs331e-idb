import React, { Component } from "react";

import PacMan from "./../Pacman.svg";
import PlatformInstance from "./PlatformInstance";

export class PlatformDetail extends Component {
  state = {
    slug: this.props.match.params.plat_slug,
    platData: null,
    loading: true,
  };
  async componentDidMount() {
    const url = "/api/platforms";
    const response = await fetch(url);
    const data = await response.json();
    let cur = data.filter((platform) => platform.slug === this.state.slug);
    this.setState({ platData: cur[0], loading: false });
  }

  render() {
    return (
      <React.Fragment>
        {this.state.loading || !this.state.platData ? (
          <div>
            <img src={PacMan}></img>
            Loading...
          </div>
        ) : (
          <div className="container">
            <PlatformInstance data={this.state.platData} />
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default PlatformDetail;
