import React, { Component } from "react";
import { MDBDataTable } from "mdbreact";
import PacMan from "./../Pacman.svg";
// * platform -generation -type -alternative name -family
class PlatformTablePage extends Component {
  handleClick = (slug) => {
    window.location.href = `platforms/${slug}`;
  };
  state = {
    loading: true,
    platforms: null,
    tableDataColumns: [
      { label: "Name", field: "name", sort: "asc", width: 150 },
      {
        label: "Manufacturer",
        field: "manufacturer",
        sort: "asc",
        width: 150,
      },
      {
        label: "Year Released",
        field: "year_released",
        sort: "asc",
        width: 150,
      },
      {
        label: "Generation",
        field: "generation",
        sort: "asc",
        width: 150,
      },
      {
        label: "Alternative Name",
        field: "alternative_name",
        sort: "asc",
        width: 150,
      },
    ],
    platformTableData: {},
  };

  async componentDidMount() {
    const url = "/api/platforms";
    const response = await fetch(url);
    let data = await response.json();
    data.forEach((platform) => {
      platform.clickEvent = () => this.handleClick(platform.slug);
    });
    this.setState({
      platforms: data,
      loading: false,
      platformTableData: { columns: this.state.tableDataColumns, rows: data },
    });
  }

  render() {
    return (
      <div id="main-table-body">
        {this.state.loading || !this.state.platforms ? (
          <div>
            <img src={PacMan}></img>
            <br></br>
            <h3>Loading...</h3>
          </div>
        ) : (
          <React.Fragment>
            <h2>Platforms</h2>
            <div id="dataTable">
              <MDBDataTable
                striped
                bordered
                hover
                small
                data={this.state.platformTableData}
                searching={true}
              />
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default PlatformTablePage;
