import React, { Component } from "react";
import PacMan from "./../Pacman.svg";
import DevInstance from "./DevInstance";
export class DevDetail extends Component {
  state = {
    slug: this.props.match.params.dev_slug,
    loading1: true,
    devData: null,
  };
  async componentDidMount() {
    const url = "/api/devs";
    const response = await fetch(url);
    const data = await response.json();
    let cur = data.filter((dev) => dev.slug === this.state.slug);
    this.setState({ devData: cur[0], loading: false });
    // console.log(this.state.devGames);
  }
  render() {
    return (
      <React.Fragment>
        {this.state.loading || !this.state.devData ? (
          <div>
            <img src={PacMan}></img>
            Loading...
          </div>
        ) : (
          <DevInstance data={this.state.devData} />
        )}
      </React.Fragment>
    );
  }
}

export default DevDetail;
