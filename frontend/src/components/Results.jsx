import React, { Component } from "react";
import { Link } from "react-router-dom";
import Pacman from "./../Pacman.svg";

export default class Results extends Component {
  state = {
    query: null,
    gameLoading: true,
    platLoading: true,
    devLoading: true,
    gameResults: null,
    devResults: null,
    platformResults: null,
  };

  async componentDidMount() {
    if (this.props.match.params.query) {
      this.setState({
        query: this.props.match.params.query.replace("%20", " "),
      });
    }
    const gameRes = await fetch("/api/games");
    const devRes = await fetch("/api/devs");
    const platformRes = await fetch("/api/platforms");
    const gameData = await gameRes.json();
    const devData = await devRes.json();
    const platformData = await platformRes.json();
    let gameResults = gameData.filter((game) =>
      game.name.toLowerCase().includes(this.state.query)
    );
    gameResults.sort((a, b) => a.name.localeCompare(b.name));
    let devResults = devData.filter((dev) =>
      dev.name.toLowerCase().includes(this.state.query)
    );
    devResults.sort((a, b) => a.name.localeCompare(b.name));
    let platformResults = platformData.filter(
      (platform) =>
        platform.name.toLowerCase().includes(this.state.query) ||
        platform.abbrev.toLowerCase().includes(this.state.query)
    );
    platformResults.sort((a, b) => a.name.localeCompare(b.name));

    this.setState({
      gameResults: gameResults,
      gameLoading: false,
      devResults: devResults,
      devLoading: false,
      platformResults: platformResults,
      platLoading: false,
    });
    console.log(gameResults);
  }
  render() {
    if (this.state.query && this.state.query.length >= 2) {
      return (
        <div className="results-div">
          <h1>Search Results for "{this.state.query}"</h1>
          <div className="card-group">
            <React.Fragment>
              {this.state.gameLoading ? (
                <React.Fragment>
                  <img src={Pacman}></img>
                  <p>Games Loading...</p>
                </React.Fragment>
              ) : (
                <div className="card">
                  <div className="card-body">
                    <h3 className="card-title">Game Results</h3>
                    {this.state.gameResults.length === 0 ? (
                      <p className="card-text">No Results found.</p>
                    ) : (
                      <React.Fragment>
                        <h4>{this.state.gameResults.length} Results</h4>
                        <ul className="card-text no-bullet">
                          {this.state.gameResults.map((game) => (
                            <li>
                              <Link to={`/games/${game.slug}`}>
                                {game.name}
                              </Link>
                            </li>
                          ))}
                        </ul>
                      </React.Fragment>
                    )}
                  </div>
                </div>
              )}
              {this.state.devLoading ? (
                <React.Fragment>
                  <img src={Pacman}></img>
                  <p>Developers Loading...</p>
                </React.Fragment>
              ) : (
                <div className="card">
                  <div className="card-body">
                    <h3 className="card-title">Developer Results:</h3>
                    {this.state.devResults.length === 0 ? (
                      <p className="card-text">No Results found</p>
                    ) : (
                      <React.Fragment>
                        <h4>{this.state.devResults.length} Results</h4>
                        <ul className="card-text no-bullet">
                          {this.state.devResults.map((dev) => (
                            <li>
                              <Link to={`/developers/${dev.slug}`}>
                                {dev.name}
                              </Link>
                            </li>
                          ))}
                        </ul>
                      </React.Fragment>
                    )}
                  </div>
                </div>
              )}
              {this.state.platLoading ? (
                <React.Fragment>
                  <img src={Pacman}></img>
                  <p>Platforms Loading...</p>
                </React.Fragment>
              ) : (
                <div className="card">
                  <div className="card-body">
                    <h3 className="card-title">Platform Results:</h3>
                    {this.state.platformResults.length === 0 ? (
                      <p className="card-text">No Results found</p>
                    ) : (
                      <React.Fragment>
                        <h4>{this.state.platformResults.length} Results</h4>
                        <ul className="card-text no-bullet">
                          {this.state.platformResults.map((plat) => (
                            <li>
                              <Link to={`/developers/${plat.slug}`}>
                                {plat.name}
                              </Link>
                            </li>
                          ))}
                        </ul>
                      </React.Fragment>
                    )}
                  </div>
                </div>
              )}
            </React.Fragment>
          </div>
        </div>
      );
    } else {
      return (
        <div className="results-div">
          <h2>Sorry, searches must be 2 or more characters.</h2>
        </div>
      );
    }
  }
}
