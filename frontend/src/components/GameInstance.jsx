import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import PacMan from "./../Pacman.svg";
const GameInstance = (props) => {
  const [loading, setLoading] = useState(true);
  const [loading2, setLoading2] = useState(true);
  const [dev, setDev] = useState(null);
  const [platforms, setPlatforms] = useState([]);
  const {
    image_id,
    slug,
    name,
    height,
    width,
    first_release_date,
    hypes,
    age_ratings,
    total_rating,
  } = props.data;
  const image = {
    url: `http://images.igdb.com/igdb/image/upload/t_1080p_2x/${image_id}.png`,
  };
  useEffect(() => {
    let setData = async () => {
      await fetch(`/api/game-dev/${slug}`)
        .then((response) => response.json())
        .then((data) => {
          setDev(data);
          setLoading(false);
          console.log(data);
        });
    };
    let setPlatformData = async () => {
      await fetch(`/api/game-platforms/${slug}`)
        .then((response) => response.json())
        .then((data) => {
          setPlatforms(data);
          setLoading2(false);
        });
    };
    setPlatformData();
    setData();
  }, []);
  return (
    <div className="game-div">
      {/* {console.log(image.url)} */}
      <h2>{name}</h2>
      <img
        src={image.url}
        alt="Could Not Load Image"
        className="game-cover"
      ></img>

      <h2>First Release Date: {first_release_date}</h2>
      <h2>Hype Score: {hypes === null ? 0 : hypes}</h2>
      <h2>ESRB: {age_ratings}</h2>
      <h2>Metacritic Score: {total_rating.toFixed(1)}</h2>
      {loading || loading2 ? (
        <React.Fragment>
          <img src={PacMan}></img>
          <p>Loading</p>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <h2>
            Developer:{" "}
            <Link to={`/developers/${dev.dev_slug}`}>{dev.dev_name}</Link>
          </h2>
          <h2>Available on:</h2>
          {platforms.map((platform) => (
            <h3>
              <Link to={`/platforms/${platform.platform_slug}`}>
                {platform.platform_name}
              </Link>
            </h3>
          ))}
        </React.Fragment>
      )}
    </div>
  );
};

export default GameInstance;
