import React, { Component } from "react";

import PropTypes from "prop-types";
import PacMan from "./../Pacman.svg";
import GameInstance from "./GameInstance";

export class GameDetail extends Component {
  state = {
    slug: this.props.match.params.game_slug,
    gameData: {},
    devData: null,
    loading: true,
  };
  async componentDidMount() {
    const url = "/api/games";
    const response = await fetch(url);
    const data = await response.json();
    let curGame = data.filter((game) => game.slug === this.state.slug);
    this.setState({ gameData: curGame[0], loading: false });
    // console.log(this.state.gameData);
  }

  render() {
    return (
      <React.Fragment>
        {this.state.loading ? (
          <div>
            <img src={PacMan}></img>
            Loading...
          </div>
        ) : (
          <div className="container">
            <GameInstance data={this.state.gameData} />
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default GameDetail;
