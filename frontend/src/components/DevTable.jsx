import React, { Component } from "react";
import { MDBDataTable } from "mdbreact";
import PacMan from "./../Pacman.svg";

class DevTable extends Component {
  handleClick = (slug) => {
    window.location.href = `/developers/${slug}`;
  };
  state = {
    loading: true,
    devs: null,
    tableDataColumns: [
      {
        label: "Name",
        field: "name",
        sort: "asc",
        width: 300,
      },
      {
        label: "Country of Origin",
        field: "country",
        sort: "asc",
        width: 150,
      },
      {
        label: "No. of Games Developed",
        field: "games_developed",
        sort: "asc",
        width: 20,
      },
      {
        label: "Average Game Rating",
        field: "average_rating",
        sort: "asc",
        width: 20,
      },
      {
        label: "Year of First Game Release",
        field: "first_release_date",
        sort: "asc",
        width: 100,
      },
    ],
    devData: {},
  };
  async componentDidMount() {
    const url = "/api/devs";
    const response = await fetch(url);
    let data = await response.json();
    data.forEach((dev) => {
      dev.average_rating = dev.average_rating.toFixed(2);
      if (dev.first_release_date === null) {
        dev.first_release_date = "NA";
      } else {
        dev.first_release_date = parseInt(dev.first_release_date);
      }
      dev.clickEvent = () => this.handleClick(dev.slug);
    });
    this.setState({
      devs: data,
      loading: false,
      devData: { columns: this.state.tableDataColumns, rows: data },
    });
  }

  render() {
    return (
      <div id="main-table-body">
        {this.state.loading || !this.state.devs ? (
          <div>
            <img src={PacMan}></img>
            <br></br>
            <h3>Loading...</h3>
          </div>
        ) : (
          <React.Fragment>
            <h2>Developers</h2>
            <div id="dataTable">
              <MDBDataTable
                striped
                bordered
                hover
                small
                data={this.state.devData}
                searching={true}
              />
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default DevTable;
