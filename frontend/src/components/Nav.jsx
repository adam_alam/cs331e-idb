import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import GameArchiveLogo from "./../logo.png";
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";

class Navbar1 extends Component {
  state = {
    query: null,
  };
  handleChange(e) {
    const name = e.target.name;
    // const value = e.target.value.toLowerCase().replace(/\s+/g, "-");
    const value = encodeURI(e.target.value.toLowerCase()).replace("/", "");
    console.log(value);
    this.setState({ query: value });
  }

  searchRedirect() {
    return <Redirect to={`/search/${this.state.query}`}></Redirect>;
  }

  render() {
    return (
      <React.Fragment>
        <Navbar expand="lg" className="navbar-custom">
          <Navbar.Brand href="/">
            <img
              src={GameArchiveLogo}
              width={40}
              height={40}
              className="d-inline-block align-top"
            />
            GameArchive
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <Nav.Link href="/games">Games</Nav.Link>
              <Nav.Link href="/developers">Developers</Nav.Link>
              <Nav.Link href="/platforms">Platforms</Nav.Link>
              <Nav.Link href="/about_us" className="text-nowrap">
                About Us
              </Nav.Link>
            </Nav>
            <Form
              inline
              onSubmit={(e) => {
                e.preventDefault();
                window.location.href = `/search/${String(this.state.query)}`;
              }}
            >
              <FormControl
                type="text"
                placeholder="Search"
                className="mr-lg-2"
                onChange={(e) => this.handleChange(e)}
              />
            </Form>
          </Navbar.Collapse>
        </Navbar>
      </React.Fragment>
    );
  }
}

export default Navbar1;
