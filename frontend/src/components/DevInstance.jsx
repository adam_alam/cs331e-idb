import React, { Component } from "react";
import PacMan from "./../Pacman.svg";
import { Link } from "react-router-dom";
class DevInstance extends Component {
  state = {
    gamesLoading: true,
    platformsLoading: true,
    games: null,
    platforms: null,
  };
  async componentDidMount() {
    const url1 = `/api/game-dev/${this.props.data.id}`;
    const url2 = `/api/game-platforms/`;
    const response2 = await fetch(url2);
    const data2 = await response2.json();
    const response1 = await fetch(url1);
    const data = await response1.json();

    this.setState({ games: data, gamesLoading: false, platforms: data2 });
    const filtered = data2.filter(
      (game) => game.game_slug === this.state.games.game_slug
    );
    // this.setState({ platforms: filtered });
  }
  render() {
    const {
      name,
      image_id,
      id,
      country,
      average_rating,
      first_release_date,
    } = this.props.data;

    return (
      <div className="dev-div">
        <h1>{name}</h1>
        <img
          className="dev-logo"
          src={`http://images.igdb.com/igdb/image/upload/t_1080p_2x/${image_id}.png`}
        ></img>

        <h3>
          First Release:{" "}
          {first_release_date === null ? "NA" : first_release_date}
        </h3>
        <h3>Origin: {country}</h3>
        <h3>Average Rating: {average_rating.toFixed(2)}</h3>
        {this.state.gamesLoading ? (
          <React.Fragment>
            <img src={PacMan}></img>
            <p>Loading</p>
          </React.Fragment>
        ) : (
          <div>
            <div className="container">
              <div className="row">
                {this.state.games.map((game) => (
                  <div className="col-md-6 col-lg-4">
                    <div className="card">
                      <img
                        className="card-img-top"
                        src={`http://images.igdb.com/igdb/image/upload/t_1080p_2x/${game.image_id}.png`}
                        alt="Card image cap"
                      />
                      <div className="card-body">
                        <h5 className="card-title">{game.game_name}</h5>
                        <p className="card-text">{game.game_year}</p>
                        <Link
                          to={`/games/${game.game_slug}`}
                          className="btn btn-primary"
                        >
                          View {game.game_name}
                        </Link>

                        <ul className="dev-plat-list">
                          <br />
                          <h3>Platforms:</h3>
                          {this.state.platforms
                            .filter((obj) => obj.game_id === game.game_id)
                            .map((obj) => (
                              <li>
                                <Link to={`/platforms/${obj.platform_slug}`}>
                                  {obj.platform_name}
                                </Link>
                              </li>
                            ))}
                        </ul>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default DevInstance;
