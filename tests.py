#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/main.py
# Fares Fraij
# ---------------------------

# -------
# imports
# -------

import os
import sys
import unittest
from models import app, db, Platform, Platform_families, Platform_logos, \
     Game, Game_genre, Covers, Game_platforms, Game_company, \
     Genres, Companies, Company_logos #Company_countries, Category, Game_age,


# -----------
# DBTestCases
# -----------

class DBTestCases(unittest.TestCase):
    
    # ---------
    # platformFamilies
    # ---------
    def test_01(self):
        newPlatform_families = Platform_families(id=5, name="Nintendo", slug="nintendo")
        
        db.session.add(newPlatform_families)
        db.session.commit()

        r = db.session.query(Platform_families).filter_by(id = 5).one()
        self.assertEqual(str(r.id), '5')
        self.assertEqual(str(r.name), 'Nintendo')
        self.assertEqual(str(r.slug), 'nintendo')

    def test_02(self):
        newPlatform_families = Platform_families(id=8, name="Playstation 4", slug="ps4")
        
        db.session.add(newPlatform_families)
        db.session.commit()

        r = db.session.query(Platform_families).filter_by(id = 8).one()
        self.assertEqual(str(r.id), '8')
        self.assertEqual(str(r.name), 'Playstation 4')
        self.assertEqual(str(r.slug), 'ps4')

    def test_03(self):
        newPlatform_families = Platform_families(id=34, name="Xbox", slug="xb")
        
        db.session.add(newPlatform_families)
        db.session.commit()

        r = db.session.query(Platform_families).filter_by(id = 34).one()
        self.assertEqual(str(r.id), '34')
        self.assertEqual(str(r.name), 'Xbox')
        self.assertEqual(str(r.slug), 'xb')

    # ---------
    # platformLogos
    # ---------
    def test_04(self):
        newPlatform_logos = Platform_logos(id=200, alpha_channel=True,
                            animated=False, height=1200, image_id='plfl', width=1200,
                            url="//images.igdb.com/igdb/image/upload/t_thumb/plfl.jpg")
        
        db.session.add(newPlatform_logos)
        db.session.commit()

        r = db.session.query(Platform_logos).filter_by(id = 200).one()
        self.assertEqual(str(r.id), '200')
        self.assertEqual(str(r.alpha_channel), 'True')
        self.assertEqual(str(r.width), '1200')

    def test_05(self):
        newPlatform_logos = Platform_logos(id=121, alpha_channel=False,
                            animated=False, height=1200, image_id='phds', width=1200,
                            url="//images.igdb.com/igdb/image/upload/t_thumb/phds.jpg")
        
        db.session.add(newPlatform_logos)
        db.session.commit()

        r = db.session.query(Platform_logos).filter_by(id = 121).one()
        self.assertEqual(str(r.id), '121')
        self.assertEqual(str(r.animated), 'False')
        self.assertEqual(str(r.height), '1200')

    def test_06(self):
        newPlatform_logos = Platform_logos(id=54, alpha_channel=False,
                            animated=True, height=1200, image_id='lmk', width=1200,
                            url="//images.igdb.com/igdb/image/upload/t_thumb/lmk.jpg")
        
        db.session.add(newPlatform_logos)
        db.session.commit()

        r = db.session.query(Platform_logos).filter_by(id = 54).one()
        self.assertEqual(str(r.id), '54')
        self.assertEqual(str(r.animated), 'True')
        self.assertEqual(str(r.url), '//images.igdb.com/igdb/image/upload/t_thumb/lmk.jpg')
    '''
    # ---------
    # gameAge
    # ---------
    def test_07(self):
        newGameAge = Game_age(id = 2, rating="R")

        db.session.add(newGameAge)
        db.session.commit()

        r = db.session.query(Game_age).filter_by(id = 2).one()
        self.assertEqual(str(r.id), '2')
        self.assertEqual(str(r.rating), 'R')

    def test_08(self):
        newGameAge = Game_age(id = 3, rating="PG13")

        db.session.add(newGameAge)
        db.session.commit()

        r = db.session.query(Game_age).filter_by(id = 3).one()
        self.assertEqual(str(r.id), '3')
        self.assertEqual(str(r.rating), 'PG13')

    def test_09(self):
        newGameAge = Game_age(id = 4, rating="PG")

        db.session.add(newGameAge)
        db.session.commit()

        r = db.session.query(Game_age).filter_by(id = 4).one()
        self.assertEqual(str(r.id), '4')
        self.assertEqual(str(r.rating), 'PG')
    '''
    
    # ---------
    # covers
    # ---------
    def test_10(self):
        newCovers = Covers(id=4, height=200, image_id="ghg", url="//image_url", width=300)

        db.session.add(newCovers)
        db.session.commit()

        r = db.session.query(Covers).filter_by(id = 4).one()
        self.assertEqual(str(r.id), '4')
        self.assertEqual(str(r.height), '200')
        self.assertEqual(str(r.url), '//image_url')
        self.assertEqual(str(r.width), '300')

    def test_11(self):
        newCovers = Covers(id=6, height=500, image_id="ftfj", url="//image_url/ftfj", width=250)

        db.session.add(newCovers)
        db.session.commit()

        r = db.session.query(Covers).filter_by(id = 6).one()
        self.assertEqual(str(r.id), '6')
        self.assertEqual(str(r.image_id), 'ftfj')
        self.assertEqual(str(r.width), '250')

    def test_12(self):
        newCovers = Covers(id=7, height=300, image_id="gfgk", url="//image_url/gfgk", width=700)

        db.session.add(newCovers)
        db.session.commit()

        r = db.session.query(Covers).filter_by(id = 7).one()
        self.assertEqual(str(r.id), '7')
        self.assertEqual(str(r.height), '300')
        self.assertEqual(str(r.image_id), 'gfgk')

    # ---------
    # genres
    # ---------
    def test_13(self):
        newGenres = Genres(id=1, name="shooting", slug="shoot")

        db.session.add(newGenres)
        db.session.commit()

        r = db.session.query(Genres).filter_by(id = 1).one()
        self.assertEqual(str(r.id), '1')
        self.assertEqual(str(r.name), 'shooting')
        self.assertEqual(str(r.slug), 'shoot')

    def test_14(self):
        newGenres = Genres(id=2, name="racing", slug="race")

        db.session.add(newGenres)
        db.session.commit()

        r = db.session.query(Genres).filter_by(id = 2).one()
        self.assertEqual(str(r.id), '2')
        self.assertEqual(str(r.name), 'racing')
        self.assertEqual(str(r.slug), 'race')

    def test_15(self):
        newGenres = Genres(id=3, name="puzzle", slug="puzzle")

        db.session.add(newGenres)
        db.session.commit()

        r = db.session.query(Genres).filter_by(id = 3).one()
        self.assertEqual(str(r.id), '3')
        self.assertEqual(str(r.name), 'puzzle')
        self.assertEqual(str(r.slug), 'puzzle')

    # ---------
    # CompanyLogos
    # ---------
    def test_16(self):
        newLogos = Company_logos(id=1, alpha_channel=True, 
                    animated=True, height=200, image_id="img1",
                    url="image/url/img1", width=150)
        
        db.session.add(newLogos)
        db.session.commit()

        r = db.session.query(Company_logos).filter_by(id = 1).one()
        self.assertEqual(str(r.id), '1')
        self.assertEqual(str(r.alpha_channel), 'True')
        self.assertEqual(str(r.width), '150')

    def test_17(self):
        newLogos = Company_logos(id=2, alpha_channel=False, 
                    animated=True, height=300, image_id="img2",
                    url="image/url/img2", width=800)
        
        db.session.add(newLogos)
        db.session.commit()

        r = db.session.query(Company_logos).filter_by(id = 2).one()
        self.assertEqual(str(r.id), '2')
        self.assertEqual(str(r.animated), 'True')
        self.assertEqual(str(r.height), '300')

    def test_18(self):
        newLogos = Company_logos(id=3, alpha_channel=True, 
                    animated=False, height=500, image_id="img3",
                    url="image/url/img3", width=500)
        
        db.session.add(newLogos)
        db.session.commit()

        r = db.session.query(Company_logos).filter_by(id = 3).one()
        self.assertEqual(str(r.id), '3')
        self.assertEqual(str(r.image_id), 'img3')
        self.assertEqual(str(r.url), 'image/url/img3')
    '''
    # ---------
    # companyCountries
    # ---------
    def test_19(self):
        newCountries = Company_countries(id=1, country="US")

        db.session.add(newCountries)
        db.session.commit()

        r = db.session.query(Company_countries).filter_by(id = 1).one()
        self.assertEqual(str(r.id), '1')
        self.assertEqual(str(r.country), 'US')

    def test_20(self):
        newCountries = Company_countries(id=2, country="Mexico")

        db.session.add(newCountries)
        db.session.commit()

        r = db.session.query(Company_countries).filter_by(id = 2).one()
        self.assertEqual(str(r.id), '2')
        self.assertEqual(str(r.country), 'Mexico')

    def test_21(self):
        newCountries = Company_countries(id=3, country="France")

        db.session.add(newCountries)
        db.session.commit()

        r = db.session.query(Company_countries).filter_by(id = 3).one()
        self.assertEqual(str(r.id), '3')
        self.assertEqual(str(r.country), 'France')
    '''
    # ---------
    # platform
    # ---------
    def test_22(self):
        newPlatform = Platform(id=1, abbreviation='nintendo_ds', alternative_name='ds',
                       platform_logo=200, slug='nint', platform_family=5,
                       generation=2, category=1, name='nintendo ds',
                       year_released=2020, manufacturer="Nintendo")
        
        db.session.add(newPlatform)
        db.session.commit()

        r = db.session.query(Platform).filter_by(id = 1).one()
        self.assertEqual(str(r.id), '1')
        self.assertEqual(str(r.name), 'nintendo ds')
        self.assertEqual(str(r.abbreviation), 'nintendo_ds')
        self.assertEqual(str(r.category), '1')
        self.assertEqual(str(r.year_released), '2020')

    def test_23(self):
        newPlatform = Platform(id=5, abbreviation='ps', alternative_name='ps4',
                       platform_logo=121, slug='ps4', platform_family=8,
                       generation=1, category=3, name='Playstation 4',
                       year_released=2015, manufacturer="Sony")
        
        db.session.add(newPlatform)
        db.session.commit()

        r = db.session.query(Platform).filter_by(id = 5).one()
        self.assertEqual(str(r.id), '5')
        self.assertEqual(str(r.alternative_name), 'ps4')
        self.assertEqual(str(r.slug), 'ps4')
        self.assertEqual(str(r.generation), '1')
        self.assertEqual(str(r.manufacturer), 'Sony')

    def test_24(self):
        newPlatform = Platform(id=3, abbreviation='pc', alternative_name='pc',
                       platform_logo=54, slug='pc', platform_family=34,
                       generation=8, category=3, name='Windows',
                       year_released=1985, manufacturer="Microsoft")
        
        db.session.add(newPlatform)
        db.session.commit()

        r = db.session.query(Platform).filter_by(id = 3).one()
        self.assertEqual(str(r.id), '3')
        self.assertEqual(str(r.alternative_name), 'pc')
        self.assertEqual(str(r.name), 'Windows')
        self.assertEqual(str(r.platform_family), '34')
        self.assertEqual(str(r.manufacturer), 'Microsoft')
        self.assertEqual(str(r.year_released), '1985')

    # ---------
    # games
    # ---------
    def test_25(self):
        newGame = Game(id=22, age_ratings="R", name="Super Smash Bros", slug="smash bros",
                       total_rating=80.4, cover=4,
                       first_release_date = 2004, hypes=102)
        
        db.session.add(newGame)
        db.session.commit()

        r = db.session.query(Game).filter_by(id = 22).one()
        self.assertEqual(str(r.id), '22')
        self.assertEqual(str(r.name), 'Super Smash Bros')
        self.assertEqual(str(r.age_ratings), 'R')
        self.assertEqual(str(r.slug), 'smash bros')

    def test_26(self):
        newGame = Game(id=23, age_ratings="None", name="Call of Duty", slug="cod",
                       total_rating=76.4, cover=6,
                       first_release_date = 2009, hypes=124)
        
        db.session.add(newGame)
        db.session.commit()

        r = db.session.query(Game).filter_by(id = 23).one()
        self.assertEqual(str(r.id), '23')
        self.assertEqual(str(r.total_rating), '76.4')
        self.assertEqual(str(r.cover), '6')
        self.assertEqual(str(r.slug), 'cod')

    def test_27(self):
        newGame = Game(id=24, age_ratings="R", name="Need for Speed", slug="speed",
                       total_rating=54.2, cover=7,
                       first_release_date = 2015, hypes=98)
        
        db.session.add(newGame)
        db.session.commit()

        r = db.session.query(Game).filter_by(id = 24).one()
        self.assertEqual(str(r.id), '24')
        self.assertEqual(str(r.name), 'Need for Speed')
        self.assertEqual(str(r.hypes), '98')

    # ---------
    # gameGenre
    # ---------
    def test_28(self):
        newGameGenre = Game_genre(id=22, genre=1)

        db.session.add(newGameGenre)
        db.session.commit()

        r = db.session.query(Game_genre).filter_by(id = 22).one()
        self.assertEqual(str(r.id), '22')
        self.assertEqual(str(r.genre), '1')

    def test_29(self):
        newGameGenre = Game_genre(id=23, genre=2)

        db.session.add(newGameGenre)
        db.session.commit()

        r = db.session.query(Game_genre).filter_by(id = 23).one()
        self.assertEqual(str(r.id), '23')
        self.assertEqual(str(r.genre), '2')

    def test_30(self):
        newGameGenre = Game_genre(id=24, genre=3)

        db.session.add(newGameGenre)
        db.session.commit()

        r = db.session.query(Game_genre).filter_by(id = 24).one()
        self.assertEqual(str(r.id), '24')
        self.assertEqual(str(r.genre), '3')

    # ---------
    # gamePlatforms
    # ---------
    def test_31(self):
        newGamePlatform = Game_platforms(game=22, platform=1)

        db.session.add(newGamePlatform)
        db.session.commit()

        r = db.session.query(Game_platforms).filter_by(game = 22).one()
        self.assertEqual(str(r.game), '22')
        self.assertEqual(str(r.platform), '1')

    def test_32(self):
        newGamePlatform = Game_platforms(game=23, platform=5)

        db.session.add(newGamePlatform)
        db.session.commit()

        r = db.session.query(Game_platforms).filter_by(game = 23).one()
        self.assertEqual(str(r.game), '23')
        self.assertEqual(str(r.platform), '5')

    def test_33(self):
        newGamePlatform = Game_platforms(game=24, platform=3)

        db.session.add(newGamePlatform)
        db.session.commit()

        r = db.session.query(Game_platforms).filter_by(game = 24).one()
        self.assertEqual(str(r.game), '24')
        self.assertEqual(str(r.platform), '3')

    # ---------
    # Companies
    # ---------
    def test_34(self):
        newCompanies = Companies(id=1, country="US", name="Nintendo", slug="nint",
                        first_release_date=1980, logo=2, \
                        games_developed=22)
        
        db.session.add(newCompanies)
        db.session.commit()

        r = db.session.query(Companies).filter_by(id = 1).one()
        self.assertEqual(str(r.id), '1')
        self.assertEqual(str(r.country), 'US')
        self.assertEqual(str(r.name), 'Nintendo')

    def test_35(self):
        newCompanies = Companies(id=2, country="France", name="Microsoft", slug="msft",
                        first_release_date=1964, logo=3, \
                        games_developed=47)
        
        db.session.add(newCompanies)
        db.session.commit()

        r = db.session.query(Companies).filter_by(id = 2).one()
        self.assertEqual(str(r.id), '2')
        self.assertEqual(str(r.country), 'France')
        self.assertEqual(str(r.slug), 'msft')

    def test_36(self):
        newCompanies = Companies(id=3, country="Mexico", name="Sony", slug="sony",
                        first_release_date=1987, logo=1, \
                        games_developed=19)
        
        db.session.add(newCompanies)
        db.session.commit()

        r = db.session.query(Companies).filter_by(id = 3).one()
        self.assertEqual(str(r.id), '3')
        self.assertEqual(str(r.first_release_date), '1987')
        self.assertEqual(str(r.logo), '1')

    # ---------
    # gameCompany
    # ---------
    def test_37(self):
        newGameCompany = Game_company(game=22, company=3)

        db.session.add(newGameCompany)
        db.session.commit()

        r = db.session.query(Game_company).filter_by(game = 22).one()
        self.assertEqual(str(r.game), '22')
        self.assertEqual(str(r.company), '3')

    def test_38(self):
        newGameCompany = Game_company(game=23, company=2)

        db.session.add(newGameCompany)
        db.session.commit()

        r = db.session.query(Game_company).filter_by(game = 23).one()
        self.assertEqual(str(r.game), '23')
        self.assertEqual(str(r.company), '2')

    def test_39(self):
        newGameCompany = Game_company(game=24, company=1)

        db.session.add(newGameCompany)
        db.session.commit()

        r = db.session.query(Game_company).filter_by(game = 24).one()
        self.assertEqual(str(r.game), '24')
        self.assertEqual(str(r.company), '1')
    
    
    # ---------
    # delete
    # ---------
    def test_remove_all(self):

        db.session.query(Game_company).filter_by(game = 22).delete()
        db.session.query(Game_company).filter_by(game = 23).delete()
        db.session.query(Game_company).filter_by(game = 24).delete()
        db.session.commit()

        db.session.query(Companies).filter_by(id = 1).delete()
        db.session.query(Companies).filter_by(id = 2).delete()
        db.session.query(Companies).filter_by(id = 3).delete()
        db.session.commit()

        db.session.query(Game_platforms).filter_by(game = 22).delete()
        db.session.query(Game_platforms).filter_by(game = 23).delete()
        db.session.query(Game_platforms).filter_by(game = 24).delete()
        db.session.commit()

        db.session.query(Game_genre).filter_by(id = 22).delete()
        db.session.query(Game_genre).filter_by(id = 23).delete()
        db.session.query(Game_genre).filter_by(id = 24).delete()
        db.session.commit()

        db.session.query(Game).filter_by(id = 22).delete()
        db.session.query(Game).filter_by(id = 23).delete()
        db.session.query(Game).filter_by(id = 24).delete()
        db.session.commit()
        
        db.session.query(Platform).filter_by(id = 1).delete()
        db.session.query(Platform).filter_by(id = 5).delete()
        db.session.query(Platform).filter_by(id = 3).delete()
        db.session.commit()

        '''db.session.query(Company_countries).filter_by(id = 1).delete()
        db.session.query(Company_countries).filter_by(id = 2).delete()
        db.session.query(Company_countries).filter_by(id = 3).delete()
        db.session.commit()'''

        db.session.query(Company_logos).filter_by(id = 1).delete()
        db.session.query(Company_logos).filter_by(id = 2).delete()
        db.session.query(Company_logos).filter_by(id = 3).delete()
        db.session.commit()

        db.session.query(Genres).filter_by(id = 1).delete()
        db.session.query(Genres).filter_by(id = 2).delete()
        db.session.query(Genres).filter_by(id = 3).delete()
        db.session.commit()

        db.session.query(Covers).filter_by(id = 4).delete()
        db.session.query(Covers).filter_by(id = 6).delete()
        db.session.query(Covers).filter_by(id = 7).delete()
        db.session.commit()

        '''db.session.query(Game_age).filter_by(id = 2).delete()
        db.session.query(Game_age).filter_by(id = 3).delete()
        db.session.query(Game_age).filter_by(id = 4).delete()
        db.session.commit()'''
        
        db.session.query(Platform_logos).filter_by(id = 200).delete()
        db.session.query(Platform_logos).filter_by(id = 121).delete()
        db.session.query(Platform_logos).filter_by(id = 54).delete()
        db.session.commit()

        db.session.query(Platform_families).filter_by(id = 5).delete()
        db.session.query(Platform_families).filter_by(id = 8).delete()
        db.session.query(Platform_families).filter_by(id = 34).delete()
        db.session.commit()


if __name__ == '__main__':
    unittest.main()
# end of code


