#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/models.py
# Fares Fraij
# ---------------------------

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
from flask_cors import CORS


app = Flask(__name__, static_folder="./frontend/build/static", template_folder="./frontend/build")
CORS(app)
#app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQLALCHEMY_DATABASE_URI')
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", 'postgresql://postgres:asd123@35.194.51.252:5432/postgres')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

# ------------
# Platform
# ------------
class Platform(db.Model):
    
    __tablename__ = 'platform'

    id = db.Column(db.Integer, primary_key = True)
    abbreviation = db.Column(db.String(80))
    alternative_name = db.Column(db.String(80))
    category = db.Column(db.String(80)) #, db.ForeignKey('category.id')
    platform_family = db.Column(db.Integer, db.ForeignKey('platform_families.id'))
    platform_logo = db.Column(db.Integer, db.ForeignKey('platform_logos.id'))
    slug = db.Column(db.String(80))
    generation = db.Column(db.Integer)
    name = db.Column(db.String(80), nullable = False)
    year_released = db.Column(db.Integer)
    manufacturer = db.Column(db.String(80))
    platform_image_url = db.Column(db.String(200))

    fk_platform = db.relationship('Game_platforms', backref = 'fk_platform')


# ------------
# Platform Families
# ------------
class Platform_families(db.Model):
    
    __tablename__ = 'platform_families'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(80), nullable = False)
    slug = db.Column(db.String(80))

    fk_platform_families = db.relationship('Platform', backref = 'fk_platform_families', uselist = False)


# ------------
# Platform Logos
# ------------
class Platform_logos(db.Model):
    
    __tablename__ = 'platform_logos'

    id = db.Column(db.Integer, primary_key = True)
    alpha_channel = db.Column(db.Boolean)
    animated = db.Column(db.Boolean)
    height = db.Column(db.Integer)
    image_id = db.Column(db.String(80))
    url = db.Column(db.String(80), nullable = False)
    width = db.Column(db.Integer)

    fk_platform_logos = db.relationship('Platform', backref = 'fk_platform_logos', uselist = False)

'''
# ------------
# Category
# ------------
class Category(db.Model):
    
    __tablename__ = 'category'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(80), nullable = False)

    fk_category = db.relationship('Platform', backref = 'fk_category', uselist = False)
'''    

# ------------
# Game
# ------------
class Game(db.Model):
    
    __tablename__ = 'game'

    id = db.Column(db.Integer, primary_key = True)
    age_ratings = db.Column(db.String(80))
    name = db.Column(db.String(80))
    slug = db.Column(db.String(80))
    total_rating = db.Column(db.Float)       ##Double ?
    cover = db.Column(db.Integer, db.ForeignKey('covers.id'))
    first_release_date = db.Column(db.Integer) ##Datetime ?
    hypes = db.Column(db.Integer)

    fk_game_genre = db.relationship('Game_genre', backref = 'fk_game_genre')
    fk_game_platforms = db.relationship('Game_platforms', backref = 'fk_game_platforms')
    fk_game_company = db.relationship('Game_company', backref = 'fk_game_company')

'''   
# ------------
# Game Age Ratings
# ------------
class Game_age(db.Model):

    __tablename__ = 'game_age'

    id = db.Column(db.Integer, primary_key = True)
    rating = db.Column(db.String(80))

    fk_age = db.relationship('Game', backref = 'fk_age')
'''

# ------------
# Game Genre
# ------------
class Game_genre(db.Model):

    __tablename__ = 'game_genre'

    id = db.Column(db.Integer, db.ForeignKey('game.id'), primary_key = True)
    genre = db.Column(db.Integer, db.ForeignKey('genres.id'), primary_key = True)


# ------------
# Covers
# ------------
class Covers(db.Model):
    
    __tablename__ = 'covers'

    id = db.Column(db.Integer, primary_key = True)
    #alpha_channel = db.Column(db.Boolean, nullable = False)    #json doesn't have
    #animated = db.Column(db.Boolean, nullable = False)
    game_id = db.Column(db.Integer)
    height = db.Column(db.Integer)
    image_id = db.Column(db.String(80))
    url = db.Column(db.String(80))
    width = db.Column(db.Integer)

    fk_covers = db.relationship('Game', backref = 'fk_covers')


# ------------
# Game Platforms
# ------------
class Game_platforms(db.Model):

    __tablename__ = 'game_platforms'

    game = db.Column(db.Integer, db.ForeignKey('game.id'), primary_key = True)
    platform = db.Column(db.Integer, db.ForeignKey('platform.id'), primary_key = True)


# ------------
# Game Company
# ------------
class Game_company(db.Model):
    
    __tablename__ = 'game_company'

    game = db.Column(db.Integer, db.ForeignKey('game.id'), primary_key = True)
    company = db.Column(db.Integer, db.ForeignKey('companies.id'), primary_key = True)


# ------------
# Genres
# ------------
class Genres(db.Model):

    __tablename__ = 'genres'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(80), nullable = False)
    slug = db.Column(db.String(80))

    fk_genres = db.relationship('Game_genre', backref = 'fk_genres')


# ------------
# Companies
# ------------
class Companies(db.Model):

    __tablename__ = 'companies'

    id = db.Column(db.Integer, primary_key = True)
    country = db.Column(db.String(80))
    name = db.Column(db.String(80))
    slug = db.Column(db.String(80))
    first_release_date = db.Column(db.Integer) ##Datetime ?
    logo = db.Column(db.Integer, db.ForeignKey('company_logos.id'))
    games_developed = db.Column(db.Integer)
    average_rating = db.Column(db.Float)

    fk_companies = db.relationship('Game_company', backref = 'fk_companies')


# ------------
# Company Logos
# ------------
class Company_logos(db.Model):
    
    __tablename__ = 'company_logos'

    id = db.Column(db.Integer, primary_key = True)
    alpha_channel = db.Column(db.Boolean)
    animated = db.Column(db.Boolean)
    height = db.Column(db.Integer)
    image_id = db.Column(db.String(80))
    url = db.Column(db.String(80))
    width = db.Column(db.Integer)

    fk_company_logos = db.relationship('Companies', backref = 'fk_company_logos')

'''
# ------------
# Company Countries
# ------------
class Company_countries(db.Model):

    __tablename__ = 'company_countries'

    id = db.Column(db.Integer, primary_key = True)
    country = db.Column(db.String(80))

    fk_country = db.relationship('Companies', backref = 'fk_country')
'''
    
db.drop_all()
db.create_all()
