#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/create_db.py
# Fares Fraij
# ---------------------------

import json
from datetime import datetime, timedelta
from models import app, db, Platform, Platform_families, Platform_logos, \
     Game, Game_genre, Covers, Game_platforms, Game_company, \
     Genres, Companies, Company_logos #Company_countries,Category,Game_age,

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """

    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

# ------------
# create Platform Families
# ------------
def create_Platform_families():
    """
    populate platform families table
    """
    platform_families = load_json('JSON/Platforms/Platform_Families.json')

    for each in platform_families:
        id = each['id']
        name = each['name']
        slug = each['slug']


        newPlatform_families = Platform_families(id=id, name=name, slug=slug)

        # After I create the book, I can then add it to my session.
        db.session.add(newPlatform_families)
        # commit the session to my DB.
        db.session.commit()

# ------------
# create Platform Logos
# ------------
def create_Platform_logos():
    """
    populate platform logos table
    """
    platform_logos = load_json('JSON/Platforms/Platform_Logos.json')

    for each in platform_logos:
        id = each['id']
        alpha_channel = each['alpha_channel']
        animated = each['animated']
        height = each['height']
        image_id = each['image_id']
        url = each['url']
        width = each['width']

        newPlatform_logos = Platform_logos(id=id, alpha_channel=alpha_channel,
                            animated=animated, height=height, image_id=image_id,
                            url=url, width=width)

        # add it to my session.
        db.session.add(newPlatform_logos)
        # commit the session to my DB.
        db.session.commit()
'''
# ------------
# create game age ratings
# ------------
def create_game_age():
    """
    populate ages
    """
    ages = load_json('JSON/Games/Ratings.json')

    for each in ages:
        id = each['id']
        rating = each['rating']

        newRatings = Game_age(id=id, rating=rating)

        # add it to my session.
        db.session.add(newRatings)
        # commit the session to my DB.
        db.session.commit()
'''
# ------------
# create Game Covers
# ------------
def create_covers():
    """
    populate covers table
    """
    covers = load_json('JSON/Games/Covers.json')

    for each in covers:
        id = each['id']
        height = each['height']
        image_id = each['image_id']
        url = each['url']
        width = each['width']
        game_id = each['game']
        #alpha_channel  not in json
        #animated

        newCovers = Covers(id=id, height=height, image_id=image_id, url=url, width=width, game_id=game_id)

        # add it to my session.
        db.session.add(newCovers)
        # commit the session to my DB.
        db.session.commit()

# ------------
# create Genres
# ------------
def create_genres():
    """
    populate genres table
    """
    genres = load_json('JSON/Games/Genres.json')

    for each in genres:
        id = each['id']
        name = each['name']
        slug = each['slug']

        newGenres = Genres(id=id, name=name, slug=slug)

        # add it to my session.
        db.session.add(newGenres)
        # commit the session to my DB.
        db.session.commit()

# ------------
# create company logo
# ------------
def create_company_logo():
    """
    populate covers table
    """
    logos = load_json('JSON/Developers/Logos.json')

    for each in logos:
        id = each['id']
        alpha_channel = each['alpha_channel']
        animated = each['animated']
        height = each['height']
        image_id = each['image_id']
        url = each['url']
        width = each['width']

        newLogos = Company_logos(id=id, alpha_channel=alpha_channel,
                    animated=animated, height=height, image_id=image_id,
                    url=url, width=width)

        # add it to my session.
        db.session.add(newLogos)
        # commit the session to my DB.
        db.session.commit()
'''
# ------------
# create company countries
# ------------
def create_company_countries():
    """
    populate countries
    """
    countries = load_json('JSON/Developers/Countries.json')

    for each in countries:
        id = each['id']
        country = each['name']

        newCountries = Company_countries(id=id, country=country)

        # add it to my session.
        db.session.add(newCountries)
        # commit the session to my DB.
        db.session.commit()
'''
'''
# ------------
# create Category
# ------------
def create_Category():
    """
    populate category table
    """
    category = load_json('JSON/Platforms/Platforms.json')

    for each in category:
        id = each['id']
        name = each['name']     ##SAME AS PLATFORMS-ID AND NAME

        newCategory = Category(id=id, name=name)

        # add it to my session.
        db.session.add(newCategory)
        # commit the session to my DB.
        db.session.commit()
'''
# ------------
# create Platform
# ------------
def create_platform():
    """
    populate platform table
    """
    platforms = load_json('JSON/Platforms/Platforms.json')

    for each in platforms:
        id = each['id']
        abbreviation = each['abbreviation']
        if 'alternative_name' in each:
            alternative_name = each['alternative_name']
        else:
            alternative_name = None

        cat = each['category']
        if cat == 1:
            category = 'official'
        elif cat == 2:
            category = 'wikia'
        elif cat == 3:
            category == 'wikipedia'
        elif cat == 4:
            category = "facebook"
        elif cat == 5:
            category = 'twitter'
        elif cat == 6:
            category = 'twitch'
        elif cat == 8:
            category = 'instagram'
        elif cat == 9:
            category = 'youtube'
        elif cat == 10:
            category = 'iphone'
        elif cat == 11:
            category = 'ipad'
        elif cat == 12:
            category = 'android'
        elif cat == 13:
            category = 'steam'
        elif cat == 14:
            category = 'reddit'
        elif cat == 15:
            category = 'itch'
        elif cat == 16:
            category = 'epicgames'
        elif cat == 17:
            category = 'gog'
        elif cat == 18:
            category = 'discord'
        else:
            category = None

        if 'platform_family' in each:
            platform_family = each['platform_family']
        else:
            platform_family = None

        platform_logo = each['platform_logo']

        slug = each['slug']
        if each['id'] == 6:
            slug = 'PC'
        if each['id'] == 130:
            slug = 'Switch'
        if each['id'] == 48:
            slug = 'PS4'
        if each['id'] == 49:
            slug = 'XBOX-One'
        if each['id'] == 167:
            slug = 'PS5'
        if each['id'] == 169:
            slug = 'XBOX-Series'

        if 'generation' in each:
            generation = each['generation']
        else:
            generation = 1
        name = each['name']

        year_released = each['year_released']
        manufacturer = each['manufacturer']
        platform_image_url = each['platform_image_url']

        newPlatform = Platform(id=id, abbreviation=abbreviation, alternative_name=alternative_name,
                       platform_logo=platform_logo, slug=slug, platform_family=platform_family,
                       generation=generation, category=category, name=name,
                       year_released=year_released, manufacturer=manufacturer, platform_image_url=platform_image_url)

        # After I create the book, I can then add it to my session.
        db.session.add(newPlatform)
        # commit the session to my DB.
        db.session.commit()

# ------------
# create Games
# ------------
def create_games():
    """
    populate game table
    """
    game = load_json('JSON/Games/Games.json')

    for each in game:
        id = each['id']
        if 'age_rating' in each:
            age_ratings = each['age_rating']
        else:
            age_ratings = None
        name = each['name']
        slug = each['slug']
        total_rating = each['total_rating']
        cover = each['cover']
        if int(each['first_release_date']) < 0:
            first_release_date = (datetime(1970, 1, 1) - timedelta(seconds = abs(int(each['first_release_date'])))).year
        else:
            first_release_date = datetime.fromtimestamp(each['first_release_date']).year
        if 'hypes' in each:
            hypes = each['hypes']
        else:
            hypes = None

        newGame = Game(id=id, age_ratings=age_ratings, name=name, slug=slug,
                       total_rating=total_rating, cover=cover,
                       first_release_date = first_release_date, hypes=hypes)

        # add it to my session.
        db.session.add(newGame)
        # commit the session to my DB.
        db.session.commit()

# ------------
# create Game Genre
# ------------
def create_game_genre():
    """
    populate game genre table
    """
    game_genre = load_json('JSON/Games/Games.json')

    for each in game_genre:
        id = each['id']

        genre = each['genre']

        newGameGenre = Game_genre(id=id, genre=genre)

        # add it to my session.
        db.session.add(newGameGenre)
        # commit the session to my DB.
        db.session.commit()

# ------------
# create Game Platforms
# ------------
def create_game_platforms():
    """
    populate game platforms table
    """
    games = load_json('JSON/Games/Games.json')

    for each in games:
        game = each['id']
        for i in each['platforms']:

            platform = i

            newGamePlatform = Game_platforms(game=game, platform=platform)

            # add it to my session.
            db.session.add(newGamePlatform)
            # commit the session to my DB.
            db.session.commit()

# ------------
# create Companies
# ------------
def create_companies():
    """
    populate companies table
    """
    companies = load_json('JSON/Developers/Companies.json')

    for each in companies:
        id = each['id']
        if 'country' in each:
            country = each['country']
        else:
            country = None
        name = each['name']
        slug = each['slug']
        if 'start_date' in each:
            if int(each['start_date']) < 0:
                try:
                    first_release_date = (datetime(1970, 1, 1) - timedelta(seconds = abs(int(each['start_date'])))).year
                except OverflowError as error:
                    first_release_date = (datetime(1970, 1, 1) - timedelta(milliseconds = abs(int(each['start_date'])))).year
            else:
                first_release_date = datetime.fromtimestamp(each['start_date']).year
        else:
            first_release_date = None
        if 'logo' in each:
            logo = each['logo']
        else:
            logo = None

        if 'developed' in each:
            games_developed = len(each['developed'])
        else:
            games_developed = 0

        if 'average_rating' in each:
            average_rating = each['average_rating']
        else:
            average_rating = None

        newCompanies = Companies(id=id, country=country, name=name, slug=slug,
                        first_release_date=first_release_date, logo=logo, \
                        games_developed=games_developed, average_rating=average_rating)

        # add it to my session.
        db.session.add(newCompanies)
        # commit the session to my DB.
        db.session.commit()

# ------------
# create Game Company
# ------------
def create_game_company():
    """
    populate game company table
    """
    games = load_json('JSON/Games/Games.json')

    for each in games:
        game = each['id']
        company = each['developer']

        newGameCompany = Game_company(game=game, company=company)

        # add it to my session.
        db.session.add(newGameCompany)
        # commit the session to my DB.
        db.session.commit()


if __name__ == "__main__":
    create_Platform_families()
    create_Platform_logos()
    #create_game_age()
    create_covers()
    create_company_logo()
    create_genres()
    #create_company_countries()
    #create_Category()
    create_platform()
    create_games()
    create_game_genre()
    create_game_platforms()
    create_companies()
    create_game_company()
